program sten2d9ptbase
use subroutines
integer, parameter    :: dp=kind(0.d0)  
real(dp)              :: ttime,gflops,stendiag,stennext,stenctr,t_begin,t_end
real(dp),pointer      :: fa(:),fb(:)
integer               :: mainIntCount,width,height

width=WIDTHCONSTANT
height=HEIGHTCONSTANT
gflops=0.0
mainIntCount=10;

allocate(fa(width*height))
allocate(fb(width*height))

write(*, '(A,I,A,I,A,I)'),"Initializing: ", width, " x ",height," PAD=", width

call initbuf(fa,width,height)
call initbuf(fb,width,height)

if ( DBG > 0 ) then
  call dbgprint(fa,width,height)
endif

stendiag=0.00125
stennext=0.00125
stenctr=0.99;

call mytime(t_begin)
call stencil9pt_base(fa,fb,width,height,stenctr,stennext,stendiag,mainIntCount)
call mytime(t_end)

ttime=t_end-t_begin

if ( DBG > 0 ) then
  call dbgprint(fb,width,height)
endif

gflops=(width*height)*17.0*mainIntCount * 1.0e-09;

write (*,'(A,F10.3,A,F10.3,A,F10.3)') 'GFlops = ',gflops, &
               ' Secs = ',ttime,' GFlops per sec = ',gflops/ttime


end program
