module subroutines

integer,parameter :: WIDTHCONSTANT=5900
integer,parameter :: WIDTHPCONSTANT=5900
integer,parameter :: HEIGHTCONSTANT=10000
integer,parameter :: DBG=1

contains
  subroutine initbuf(fbuf,width,height)
    integer, parameter :: dp=kind(0.d0)  
    integer, intent(in) :: width,height
    real(dp), intent(out)  :: fbuf(:)
    real(dp) :: val
    integer :: x,y
    do y=1,height
      if (mod(y-1,10) .eq. 0 ) then
        val=1.0
      else
        val=0.0
      endif
      do x=1,width
         fbuf((x-1)+(y-1)*WIDTHPCONSTANT+1)=val
      enddo
    enddo
  end subroutine initbuf

  subroutine stencil9pt_base(fin,fout,width,height,ctr,next,diag,intCount)
    integer, parameter   :: dp=kind(0.d0)  
    real(dp),pointer,intent(inout) :: fin(:)
    real(dp),pointer,intent(inout) :: fout(:)
    integer, intent(in)  :: width,height,intCount
    real(dp),intent(in)  :: ctr,next,diag
    real(dp),pointer     :: ftmp(:)
    integer :: i,x,y,c,n,s,e,w,nw,ne,sw,se
    print *, "Running stencil kernel:", intCount,height,width,WIDTHPCONSTANT
    allocate(ftmp(width*height))

    do i=1,intCount
       do y=2,height-1
          ! starting center pt (avoid halo)
          c=1+(y-1)*WIDTHPCONSTANT+1
          ! offsets from center pt.
          n=c-WIDTHPCONSTANT
          s=c+WIDTHPCONSTANT

          e=c+1
          w=c-1
          nw=n-1
          ne=n+1
          sw=s-1
          se=s+1
          do x=2,width-1
             fout(c)=diag*fin(nw)+ &
                  diag*fin(ne)+ &
                  diag*fin(sw)+ &
                  diag*fin(se)+ &
                  next*fin(w)+ &
                  next*fin(e)+ &
                  next*fin(n)+ &
                  next*fin(s) + &
                  ctr*fin(c)
             c=c+1
             n=n+1
             s=s+1
             e=e+1
             w=w+1
             nw=nw+1
             ne=ne+1
             sw=sw+1
             se=se+1
          enddo
       enddo
      ftmp=>fin
      fin=>fout
      fout=>ftmp
   enddo
 end subroutine stencil9pt_base

  subroutine dbgprint(fbuf,width,height)
    real*8,intent(in) :: fbuf(:)
    integer, intent(in) :: width,height
    integer :: x,y,offset
    write(*,*) "HEAD LEFT"
    do y=1,20
      offset=(y-1)*width+1
      do x=1,10
        write(*,'(F6.3,A) ',advance="no"), fbuf(offset+x)," "
      enddo
      write(*,*) ""
    enddo
    write(*,*) "TAIL LEFT"
    do y=height-19,height
      offset=(y-1)*width+1
      do x=1,10
        write(*,'(F6.3,A) ',advance="no"), fbuf(offset+x)," "
      enddo
      write(*,*) ""
    enddo
    write(*,*) "TAIL RIGHT"
    do y=height-19,height
      offset=(y-1)*width+1
      do x=width-9,width
        write(*,'(F6.3,A) ',advance="no"), fbuf(offset+x)," "
      enddo
      write(*,*) ""
    enddo
  end subroutine dbgprint
  
  subroutine mytime(tseconds)
    real*8,intent(inout) :: tseconds
    integer*8 :: count, count_rate, count_max
    real*8 :: tsec, rate

    CALL SYSTEM_CLOCK(count, count_rate, count_max)
    tsec = count
    rate = count_rate
    tseconds = tsec / rate
  end subroutine mytime 
end module subroutines 
